---
layout: post
title: 'Assistenza Tecnica: Dove, Come e Quando'
created: 1437472573
---
Nel corso dello scorso mese è stata condotta presso i <a href="https://lugmap.linux.it/">Linux Users Groups</a> una mappatura di quelli che chiamiamo "sportelli di assistenza", ovvero le attività stabili e periodiche - a cadenza settimanale, bisettimanale o mensile - di supporto, sia tecnico che informativo, svolte nelle diverse città. Tali incontri, essendo appunto ricorrenti e dunque di facile reperibilità ed accesso, offrono degli ottimi punti di riferimento locali per coloro che cercano un aiuto per installare Linux sul proprio PC, risolvere specifici problemi, o più semplicemente per avere consigli e confrontarsi faccia a faccia con altri appassionati.

Sono stati al momento individuate 10 iniziative di tal fatta, prontamente evidenziate nel <a href="https://lugmap.linux.it/eventi">Calendario Eventi</a> della LugMap con degli appositi marker per renderle quanto più visibili ed evidenti al pubblico di passaggio.

Tali esperienze sono anche state esposte <a rel="nofollow" href="http://competenzedigitali.agid.gov.it/progetto/sportelli-di-assistenza">sul portale delle Competenze Digitali dell'Agenzia per l'Italia Digitale</a>, per valorizzarle e dargli il risalto che meritano. Del resto è soprattutto grazie ad attività di questo genere, costanti e puntuali, che si possono far accrescere non solo le competenze ma la stessa consapevolezza digitale dei cittadini.

Confidiamo che il numero di tali iniziative cresca nel tempo, e che altri Users Groups vogliano almeno sperimentare questa modalità operativa. Al fine di potenziare la presenza della comunità sul territorio, ed offrire utili momenti di incontro atti a coinvolgere un numero sempre maggiore di persone. Quel che serve è una sede (non necessariamente dedicata, anzi meglio ancora se si trova spazio presso un locale pubblico), un minimo di attrezzatura per far fronte alle richieste più disparate (chiavette USB e CD avviabili con distribuzioni GNU/Linux a 32 e 64 bit, ed un set di cacciaviti per eventuali piccoli interventi hardware), qualche volontario (più si è e meglio si possono organizzare turni per non gravare troppo su ogni singola persona), un poco di pazienza ed un pizzico di entusiasmo.
