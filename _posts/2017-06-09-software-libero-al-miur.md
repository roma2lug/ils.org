---
layout: post
title: Software Libero al MIUR
created: 1497045468
---
<p style="text-align: center">
<img src="/assets/posts/images/ruffoni_mauri_miur.jpg" alt="Delegati ILS al MIUR" style="max-width: 100%">
</p>

<p>
Giovedi 8 giugno una delegazione di rappresentanti del software e della cultura libera si è recata a Roma, presso il Ministero dell'Istruzione. Tra questi anche Matteo Ruffoni e Paolo Mauri, consiglieri di <a href="/">Italian Linux Society</a> e membri attivi delle community <a rel="nofollow" href="http://wiildos.it/">Wii Libera la Lavagna</a> e <a rel="nofollow" href="http://www.porteapertesulweb.it/">Porte Aperte sul Web</a>.
</p>

<p>
L'incontro è stato occasione per presentare alcune delle attività svolte presso le scuole italiane ed una selezione dei più rilevanti progetti software (tra cui è impossibile non citare <a rel="nofollow" href="http://www.lampschool.it/">Lampschool</a>), ma anche per prendere impegni operativi concreti ed ottenere un maggiore riconoscimento formale del software e dei contenuti aperti, e delle community di insegnanti, studenti e tecnici attive nel nostro Paese.
</p>

<p>
Un tema ricorrente della riunione, considerato forte obiettivo strategico per rafforzare la posizione del software libero nella scuola, è stato quello di una maggiore relazione tra il Ministero stesso e l'<a rel="nofollow" href="http://www.itd.cnr.it/">Istituto per le Tecnologie Didattiche</a> (ITD), ente del <a rel="nofollow" href="https://www.cnr.it/">CNR</a> e "casa" della distribuzione <a rel="nofollow" href="https://sodilinux.itd.cnr.it/">SoDiLinux</a>, finalizzato a consolidare il ruolo istituzionale di un soggetto già da tempo attivo nello sviluppo e nella promozione di strumenti a sorgente aperto.
</p>
