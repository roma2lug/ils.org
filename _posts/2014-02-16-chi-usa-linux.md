---
layout: post
title: Chi usa Linux?
created: 1392506253
---
Ad un mese dall'<a href="{% link _posts/2014-01-19-9-domande-per-2000-risposte.md %}">annuncio</a> del questionario dedicato ad investigare l'identità e le opinioni degli utenti GNU/Linux in Italia, pubblichiamo in un documento le risposte raccolte. Arricchite da qualche commento e considerazione sui non sempre così scontati risultati emersi, che invitiamo tutti a leggere e a valutare.

Il movimento freesoftware cresce ma soprattutto evolve e muta ed è importante riconoscere tale evoluzione per trarne sempre il massimo coinvolgimento da parte di tutti, coinvolgimento da cui dipende la sua stessa esistenza in qualità di community basata sulla condivisione e sulla collaborazione.

Ringraziamo tutti coloro che hanno dedicato qualche minuto alla compilazione della scheda, che lasciamo comunque <a href="https://www.linuxday.it/questionario/index.php/734973?lang-it">a disposizione</a> per tutti coloro che, capitando su queste pagine, volessero a loro volta dire la propria e fornire un ulteriore contributo.

Il documento <a href="/assets/files/UtentiLinux_2014.pdf">è scaricabile da qui in formato PDF</a>, e liberamente ridistribuibile in licenza <a rel="nofollow" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons 4.0</a>.
