---
layout: post
title: 1%
created: 1484010021
---
<p style="text-align: center">
<img src="/assets/posts/images/banner_1percento.png" alt="1%">
</p>

<p>
Laddove in tanti considerano ancora il software libero una cosa da "smanettoni" spesso ci si dimentica delle innumerevoli persone il cui lavoro è stato abilitato dai bassi costi, l'elevata possibilità di personalizzazione e l'indipendenza operativa delle soluzioni opensource. Migliaia sono i freelance e le agenzie di sviluppo che ogni giorno lavorano per realizzare e distribuire siti web, e-commerce, applicazioni e piattaforme a partire dell'enorme patrimonio di strumenti liberi disponibili in Rete, e che dunque, di fatto, vivono grazie all'opensource.
</p>

<p>
Non tutti però sono consapevoli del grande guadagno che questo patrimonio rappresenta per loro, né tantomeno della responsabilità che hanno per supportarlo, tutelarlo e mantenerlo. Magari non per velleità etiche o morali, ma per proprio interesse personale ed economico. A loro è rivolta l'iniziativa <a href="https://www.linux.it/unopercento">UnoPercento</a>, un amichevole invito ai tanti piccoli professionisti autonomi che popolano il mercato italiano ad assumere un ruolo proattivo.
</p>

<p>
L'azione proposta è quella di iniziare ad aggiungere, nelle fatture emesse verso i propri clienti, un 1% del totale, da raccogliere e destinare ai progetti liberi usati per svolgere il lavoro. In modo da riconoscere e far riconoscere un valore monetario a tali prodotti, farsi promotori di un circolo virtuoso che porta vantaggi a tutti, ed avere un impatto pratico e concreto sul sostegno dell'intero ecosistema.
</p>

<p>
L'iniziativa è puramente informale e nella pagina web sono elencati diversi modi per aderire, in modo più o meno esplicito. L'intento è quello di sensibilizzare gli operatori del settore alla sostenibilità dei propri strumenti produttivi e, dunque, della loro stessa attività professionale. Anche tu, più o meno consapevolmente, hai bisogno del software libero; il software libero ha bisogno di te.
</p>
