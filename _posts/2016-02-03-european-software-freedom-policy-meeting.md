---
layout: post
title: European Software Freedom Policy Meeting
created: 1454458002
---
Venerdi 29 gennaio, approfittando della concentrazione di operatori freesoftware a Bruxelles in occasione del <a href="https://fosdem.org/2016/" rel="nofollow">FOSDEM 2016</a>, si è svolto un incontro tra i rappresentanti delle maggiori associazioni nazionali attive in Europa dedite al sostegno ed alla promozione del software libero. E, naturalmente, Italian Linux Society non poteva mancare.

L'"European Software Freedom Policy Meeting" è stato organizzato da <a rel="nofollow" href="https://fsfe.org/">Free Software Foundation Europe</a> e <a rel="nofollow" href="http://www.openforumeurope.org/">Open Forum Europe</a> con lo scopo di favorire la reciproca conoscenza dei soggetti presenti nel Vecchio Continente, per sapere chi c'è e cosa fa, avere una panoramica collettiva dei rispettivi obiettivi, metodi, progetti, problemi ed aspettative, e stimolare l'avvio di una discussione comune estesa al di là dei confini nazionali.

Questo primo incontro è stato dedicato soprattutto alle presentazioni, ma nel breve periodo verrà instaurato un canale di comunicazione permanente tra le parti e, nel prossimo futuro, saranno allestiti altri incontri di carattere maggiormente operativo.

In virtù anche di iniziative come il <a href="{% link _posts/2016-01-18-linux-presentation-day.md %}">"Linux Presentation Day"</a> introdotto pochi giorni fa, la comunità filo-linuxara di tutta Europa si sta progressivamente muovendo verso una maggiore collaborazione e partecipazione. Il 2016 inizia in modo decisamente positivo!
