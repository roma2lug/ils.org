---
layout: post
title: Segnala i Sistemi Inoperativi!
created: 1444837155
---
Da oggi, sul sito della campagna "Sistema (In)Operativo", <a href="https://www.sistemainoperativo.it/#fattisentire">pubblichiamo le segnalazioni</a> degli utenti che hanno avuto difficoltà con il rimborso della licenza Windows pre-installata sui computer che hanno acquistato.

Tale rimborso dovrebbe essere un diritto dei consumatori, definitivamente sancito dalla Cassazione nel settembre del 2014, ma diversi distributori di PC ancora fanno resistenze nel riconoscerlo. Nell'ottobre 2014 Italian Linux Society ha co-firmato, insieme all'<a rel="nofollow" href="http://aduc.it/">Associazione per i Diritti degli Utenti e Consumatori (ADUC)</a>, una lettera destinata all'<a rel="nofollow" href="http://www.agcm.it/">Autorità Garante della Concorrenza e del Mercato</a>, cui invitiamo a continuare a mandare le proprie segnalazioni di abusi da parte di produttori e distributori poco rispettosi delle normative.

L'iniziativa di raccogliere e pubblicare tali negative esperienze anche su <a href="https://www.sistemainoperativo.it/">sistemainoperativo.it</a> è destinata a mettere maggiormente in luce i nomi delle aziende cui è consigliabile non rivolgersi per l'acquisto di un nuovo computer, in attesa che l'Authority prenda atto di tali comportamenti ed agisca di conseguenza.
