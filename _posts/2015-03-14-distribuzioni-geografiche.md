---
layout: post
title: Distribuzioni Geografiche
created: 1426304588
---
Sono stati pubblicati sulla LugMap <a href="https://lugmap.linux.it/statistiche.php">un paio di grafici</a>, destinati a mettere in evidenza la distribuzione geografica dei Linux Users Groups e degli appassionati all'argomento nel nostro Paese (estrapolando quest'ultima informazione dagli iscritti alla <a href="/newsletter">newsletter ILS</a> che hanno specificato la propria provincia di interesse). Queste mappe risultano essere, oltre che curiose da vedere, anche utili per individuare le zone in cui una maggiore presenza di promozione e sostegno "linuxaro" sarebbe più utile ed efficace.

Tra le provincie col peggior rapporto numero di persone / numero di LUG (ovvero: dove il numero di interessati è molto più grande del numero di gruppi di interesse) troviamo Palermo, Roma, Modena, Cagliari e Bologna. E tra quelle con un elevato numero di curiosi ma senza nessun LUG è impossibile non citare Barletta, Frosinone, Cuneo, Campobasso ed Enna. Queste aree si presentano come terreni fertili per la nascita di nuovi gruppi di promozione, specialmente nei centri di provincia più distanti dai rispettivi capoluoghi, che sappiano coinvolgere in primis coloro che già sono sul territorio e conoscono il tema, svolgere attività più o meno periodiche, entrare nelle scuole e negli uffici pubblici, e promuovere il software libero in tutte le forme e su tutti i canali.

A coloro che vogliano cimentarsi con la costituzione di un nuovo LUG consigliamo di sfogliare il <a href="/progetti/manuale_operativo_lug.pdf">"Manuale Operativo per la Community"</a>, e ricordiamo il <a href="https://lugmap.linux.it/radar/">LUG Radar</a> e l'opportunità di avere <a href="{% link _posts/2015-01-10-uno-per-tutti.md %}">supporto istituzionale da ILS</a>.
