---
layout: post
title: Radio-Attivi
created: 1462561796
---
<p style="text-center">
<img src="https://www.linux.it/radioattivi/images/header.png" style="width: 70%; display: block; margin: auto" />
</p>

<br/>

<p>
Da qualche tempo è in circolazione una direttiva europea il cui scopo è armonizzare le varie leggi vigenti nei diversi Paesi del Vecchio Continente in materia di comunicazioni radio (e, dunque, anche il comune wireless). Intento certo nobile, ma con un grave effetto collaterale: è assai probabile che, sul medio e lungo termine, la legge porterà i produttori di hardware ad impedire o quantomeno ostacolare il caricamento di pacchetti firmware non autorizzati e certificati sui propri dispositivi.
</p>
<p>
Un duro colpo per i numerosi progetti, spesso liberi ed opensource, che permettono di potenziare o persino trasformare gli apparati comunemente reperibili sul mercato come router ed access point semplicemente aggiornando il software di cui essi sono dotati, per poterli adattare ad ogni specifica esigenza o per implementare idee innovative di networking. Benché l'ipotesi della completa soppressione della libertà di utilizzare il software che più si preferisce possa apparire lontana, <a rel="nofollow" href="http://arstechnica.com/information-technology/2016/03/tp-link-blocks-open-source-router-firmware-to-comply-with-new-fcc-rule/">è quel che già sta succedendo negli USA</a> in virtù di una regolamentazione molto simile a quella introdotta in Europa.
</p>
<p>
Per far fronte a questo assai poco desiderabile scenario, e far conoscere al pubblico le innumerevoli opportunità che rischiamo di perdere, lanciamo oggi la campagna <a href="https://www.linux.it/radioattivi/">"Radio-Attivi"</a> ed invitiamo tutti a condividere una foto di un proprio dispositivo personalizzato con uno dei tanti firmware opensource esistenti.
</p>
<p>
Gli amici di Free Software Foundation Europe hanno <a rel="nofollow" href="https://fsfe.org/activities/radiodirective/">una pagina con ulteriori dettagli sul tema</a>, e sul sito della suddetta iniziativa è stata pubblicata <a rel="nofollow" href="https://www.linux.it/radioattivi/firmware.php">una pagina</a> con alcuni dei progetti più interessanti di firmware alternativi (per meglio capire di cosa si sta parlando, e magari per scoprire nuove possibilità per il proprio router domestico).
</p>
