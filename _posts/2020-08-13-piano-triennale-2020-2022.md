---
layout: post
title: Piano Triennale 2020-2022
created: 1597333264
---

È stato pubblicato dall'Agenzia per l'Italia Digitale (AgID) il <a rel="nofollow" href="https://www.agid.gov.it/it/agenzia/stampa-e-comunicazione/notizie/2020/08/12/il-piano-triennale-linformatica-nella-pa-2020-2022">Piano Triennale 2020-2022 per l'Informatica nella PA</a>, che presenta una serie di obiettivi e azioni da intraprendere da parte degli enti nei prossimi mesi per procedere nell'attività di digitalizzazione dei processi dell'amministrazione pubblica.

<!--more-->

In più punti del documento si trovano riferimenti alla volontà di consolidare ed estendere le buone pratiche del riuso e della pubblicazione del codice prodotto in seno alle amministrazioni, tema purtroppo marginalizzato nell'ambito del precedente Piano Nazionale Innovazione 2025 (di cui <a href="{% link _posts/2019-12-19-piano-nazionale-innovazione-2025.md %}">ILS ha partecipato alla presentazione</a>). In particolare troviamo al punto R.A.1.1a la "diffusione del modello di riuso di software tra le amministrazioni in attuazione delle Linee Guida AGID sull’acquisizione e il riuso del software per la Pubblica Amministrazione", e la roadmap prevede che entro aprile 2021 "le PA che sono titolari di software sviluppato per loro conto, eseguono il rilascio in open source in ottemperanza dell’obbligo previsto dall’art. 69 CAD e secondo le procedure indicate nelle Linee guida attuative su acquisizione e riuso del software" (CAP1.PA.LA07).

Eppure, ancora piuttosto popolari sono alcune domande in tale contesto. Come e dove pubblicare il codice? Come organizzarlo prima di renderlo pubblico? Quale licenza adottare? Le <a rel="nofollow" href="https://docs.italia.it/italia/developers-italia/lg-acquisizione-e-riuso-software-per-pa-docs/it/stabile/index.html">Linee Guida su acquisizione e riuso di software per le pubbliche amministrazioni</a> sono molto ricche da questo punto di vista, ed un nuovo documento di guida allo sviluppo e gestione di software secondo il modello open source è pianificato (dal Piano Triennale 2020-2022 stesso, cfr. CAP1.LA02) per dicembre 2020, ma - avendo già avuto modo di confrontarci negli anni con diversi enti - sappiamo che alcuni dubbi sono assai frequenti e può essere richiesto un consiglio pratico e puntuale.

Pertanto, per agevolare i soggetti che vogliono (e devono) operare la pubblicazione del proprio codice, Italian Linux Society si mette a disposizione per fornire assistenza e consulenza sugli aspetti tecnici, legali e metodologici. Nei confronti dei singoli Comuni, ma anche delle Regioni che da settembre 2020 saranno chiamate a costituire Nodi Territoriali di Competenza sul tema "Riuso e Open Source" (CAP8.PA.LA01 del Piano).

Per sottoporre una richiesta di collaborazione, o anche una semplice domanda, contatta il Consiglio Direttivo di Italian Linux Society all'indirizzo email ils-cd@linux.it .
