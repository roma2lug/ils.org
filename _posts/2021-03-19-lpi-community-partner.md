---
layout: post
title: Partnership con Linux Professional Institute
image: /assets/posts/images/lpi_ils.png
---

Italian Linux Society è da oggi Community Partner di [Linux Professional Institute](https://www.lpi.org/), l'organizzazione internazionale che non solo eroga una delle più popolari certificazioni in ambito Linux ma funge da rete e connessione per diversi soggetti, sia professionali che community, in tutto il mondo.

<!--more-->

Già da diversi anni LPI supporta attivamente il [Linux Day](https://www.linuxday.it/) e ILS, ed è giunto ora il momento di formalizzare e consolidare questa relazione e perseguirla anche in altre circostanze, in altri momenti dell'anno e su altri progetti - alcuni dei quali già in corso d'opera.

Con questa nuova partnership auspichiamo a far dialogare la community italiana col resto dello scenario internazionale, per estendere il confronto e la conoscenza anche oltre i confini del nostro Paese.
