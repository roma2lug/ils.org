---
layout: post
title: "#PublicCode"
created: 1506518956
---
Nelle scorse settimane Free Software Foundation Europe ha pubblicato <a rel="nofollow" href="https://publiccode.eu/it/openletter/">una lettera aperta</a> in cui si invitano le pubbliche amministrazioni di tutta Europa a pubblicare il codice delle applicazioni che vengono da loro commissionate e pagate coi soldi dei contribuenti, per favorire il riuso - ed il relativo risparmio economico - e la trasparenza delle istituzioni.

La campagna include <a rel="nofollow" href="https://publiccode.eu/it/#about">un ottimo video divulgativo</a> che spiega le motivazioni che dovrebbero spingere le amministrazioni ad adottare software libero ed opensource, nonché <a rel="nofollow" href="https://publiccode.eu/it/#action">una raccolta firme</a> cui vi invitiamo ad aderire per sostenere l'iniziativa.

Ricordiamo in questa occasione l'analogo appello <a href="{% link _posts/2017-06-14-appello-per-i-comuni-condividete.md %}">"Comune From Scratch"</a> lanciato a giugno da <a href="/">Italian Linux Society</a>, con cui si offre supporto tecnico e legale per le amministrazioni che vogliono pubblicare il codice delle proprie applicazioni ma vorrebbero capirne di più in merito a licenze libere e strumenti per il modello di sviluppo opensource.
