---
layout: post
title: 'Sezioni Locali: Prima Bozza'
created: 1572451902
---
Come anticipato alla <a href="{% link _posts/2018-03-29-report-lugconf-2018.md %}">LUGConf</a> tenuta a <a rel="nofollow" href="https://merge-it.net/">MERGE-it</a> nel marzo 2018, <a href="/">Italian Linux Society</a> si sta attrezzando per rivedere la sua organizzazione interna ed accogliere le cosiddette "Sezioni Locali": gruppi di appassionati che operano sul territorio per la promozione e la divulgazione di Linux e del software libero, e che possano appoggiarsi all'associazione ILS per quanto concerne le questioni burocratiche e fiscali (concessioni di spazi, sponsorizzazioni, partecipazione a bandi) pur senza l'onere di dover gestire una propria associazione.

È stata pubblicata una <a href="/sezionilocali">prima bozza di regolamento interno</a>, da integrare successivamente nello Statuto stesso di Italian Linux Society, che definisce i parametri per la costituzione di una sezione e la sua gestione all'interno dell'associazione. Le Sezioni Locali saranno attivate a gennaio 2020, e fino ad allora accoglieremo feedback e suggerimenti da parte di chi è interessato a creare una sezione nella propria città, sia esso un Linux Users Group esistente o un inedito gruppo di volontari. Per commenti e considerazioni, scrivi a <a href="mailto:ils-cd@linux.it">ils-cd@linux.it</a>.
