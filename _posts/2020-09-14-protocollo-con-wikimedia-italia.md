---
layout: post
title: Protocollo con Wikimedia Italia
created: 1600118035
---

A inizio settembre, gli amici dell'associazione <a rel="nofollow" href="https://www.wikimedia.it/">Wikimedia Italia</a> hanno pubblicato <a rel="nofollow" href="https://www.wikimedia.it/bando-per-la-realizzazione-di-spettacoli-sulla-conoscenza-libera/">un bando</a> destinato a finanziare produzioni teatrali sul tema delle libertà digitali, incluso ovviamente il software libero. Italian Linux Society è stata coinvolta come parte attiva dell'iniziativa, ed i nostri soci sono a disposizione di compagnie teatrali, attori e registi che vogliono portare in scena la vita dei protagonisti del Movimento Freesoftware ma vogliono saperne un po' di più, per capire le dinamiche e le relazioni di una storia lunga più di trent'anni: <a href="mailto:ils-cd@linux.it">scriveteci</a> per ricevere, a titolo gratuito, la nostra consulenza!

<!--more-->

In questo contesto WMI e ILS hanno stipulato un Protocollo d'Intesa destinato non solo a sancire questa collaborazione specifica ma, più in generale, a consolidare un rapporto che informalmente esiste da diversi anni. Per la promozione del software e dei contenuti liberi, nonché dell'adozione delle relative licenze, presso i rispettivi bacini di pubblico.
