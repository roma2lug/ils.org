---
layout: post
title: 2018 con ILS
created: 1545672002
---
Si avvicina la fine dell'anno, e anche stavolta proponiamo una panoramica delle attività che hanno vista coinvolta Italian Linux Society nel 2018.

<ul>
    <li><a href="{% link _posts/2018-05-14-servizi-linux-it.md %}">abbiamo messo online Servizi.Linux.it</a>, sia con l'intento di fornire una alternativa libera e opensource ai ben noti collettori di dati (ed informazioni personali) governati dai Colossi del Web sia per stimolare altri a fare altrettanto presso le proprie comunità locali</li>
    <li>abbiamo partecipato a tanti eventi, in diverse forme e con diversi scopi: a luglio abbiamo tenuto uno spazio divulgativo presso <a href="{% link _posts/2018-07-31-campus-party-italia-2018.md %}">il Campus Party di Milano</a>, ad agosto abbiamo incontrato altri membri della community freesoftware italiana <a href="{% link _posts/2018-08-09-esc-2018.md %}">all'End Summer Camp di Venezia</a>, e a settembre siamo stati <a href="https://scuola.linux.it/didattica-aperta-2018">a Didattica Aperta (a Bologna)</a></li>
    <li>una menzione a parte va a <a rel="nofollow" href="https://merge-it.net/">MERGE-it</a>, convention che a marzo ha radunato a Torino le maggiori realtà italiane operative nell'ambito delle libertà digitali. In tale sede abbiamo tenuto <a href="{% link _posts/2018-03-29-report-lugconf-2018.md %}">una LUGConf</a> assai partecipata da Linux Users Groups di tutta Italia, per conoscerci e ragionare insieme sul nostro futuro</li>
    <li>grande attenzione è stata rivolta ai <a href="https://scuola.linux.it/voting-machine-lombardia">23000 computer con Ubuntu</a> distribuiti alle scuole della Lombardia, per le quali è stata fornita documentazione ed assistenza al fine di sfruttare al meglio questa opportunità per la diffusione di Linux a scuola</li>
    <li>come ogni anno abbiamo coordinato <a href="https://www.linuxday.it/2018/">il Linux Day</a>, la maggiore manifestazione nazionale dedicata al software libero localmente organizzata da gruppi di appassionati, scuole ed aziende, giunta alla sua diciottesima edizione</li>
    <li>in vista del prossimo anno ci stiamo portando avanti con il <a href="{% link _posts/2018-12-19-fosdem-extended.md %}">FOSDEM Extended</a>, nuova iniziativa decentralizzata e federata che punta a coinvolgere professionisti e power-users linuxari</li>
</ul>

Per restare sempre aggiornati su ciò che succede nel mondo del software libero in Italia potete sottoscrivere <a href="/newsletter">la nostra newsletter</a> o seguire <a rel="nofollow" href="https://twitter.com/ItaLinuxSociety/">il nostro account Twitter</a>. Nonché, ovviamente, associarvi alla nostra associazione e godere di <a href="/iscrizione">tutti i benefici riservati ai soci ILS</a>.
