---
layout: post
title: 'Un Contributo Importante: il Tuo'
created: 1405293805
---
Seguendo lo spirito del <a href="https://www.linuxday.it/2014/">Linux Day 2014</a>, quest'anno dedicato alle Community che quotidianamente operano per arricchire e tutelare il patrimonio comune di software, contenuti ed informazioni di cui - spesso inconsapevolmente - gode la maggior parte della popolazione tecnologica, è stata oggi pubblicata sul trafficato sito <a href="https://www.linux.it/">linux.it</a> una sorta di <a href="https://www.linux.it/community#tasks">"bacheca"</a> in cui verranno pubblicati nel tempo varie richieste di aiuto e supporto a diverse attività legate in un modo o nell'altro alla diffusione di Linux e del software libero in Italia. Interventi soprattutto in forma di supporto tecnico e programmazione rivolti a chi possiede particolari competenze e vuole metterle a disposizione, ma anche di divulgazione e promozione cui tutti possono indipendentemente aderire per dare il proprio contributo concreto e pratico.

Confidiamo che lo strumento, che implementa un approccio "opensource" alle azioni stesse della comunità freesoftware e deliberatamente pubblicato su una pagina di ampio passaggio, aiuti ad intercettare nuovi volontari intenzionati ad avere un proprio ruolo attivo, non da semplici spettatori, alla rivoluzione digitale e culturale abilitata dall'Internet.

Per chi volesse da subito attivarsi con una semplice azione operativa, ricordiamo che sono disponibili i <a href="https://www.linuxday.it/2014/promozione">banner del Linux Day 2014</a>, da esporre sul proprio sito, il proprio blog, o anche condividere sui propri profili social.
