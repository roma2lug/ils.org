---
layout: post
title: Help Desk
created: 1591265788
---
<a href="https://www.linux.it/">Linux.it</a>, il principale sito mantenuto da Italian Linux Society, riceve ogni giorno centinaia di visite. In massima parte di tratta di persone che vogliono saperne qualcosa di più su Linux e sul software libero ma spesso capita anche qualcuno che cerca supporto per un particolare problema tecnico o, più semplicemente, vuole una informazione specifica non documentata sul sito stesso.

<!--more-->

La pagina dei contatti invita esplicitamente a cercare il Linux Users Group più vicino a casa propria per questo genere di assistenza, e ancora meglio uno degli sportelli periodici di incontro <a href="https://lugmap.linux.it/eventi/">mappati sulla LugMap</a>, ma non sempre c'è un gruppo di supporto abbastanza vicino o abbastanza comodo e le richieste finiscono con l'essere inviate ad uno degli indirizzi email di Italian Linux Society.

Per far fronte a tali richieste e cercare di accontentare tutti è stato istituito un servizio di Help Desk online mantenuto dai volontari dell'associazione ILS: scrivendo all'indirizzo <img src="/assets/images/assistenza.png" style="height: 25px; width: auto; margin-top: -7px"> il proprio messaggio viene inoltrato ad un sistema di ticketing, preso in carico da uno dei volontari, e - con l'aiuto di tutti - gestito nel più breve tempo possibile.

Speriamo in questo modo di non lasciare nessuno senza il dovuto aiuto nella propria esperienza con Linux, e di contribuire a far crescere sempre più l'utilizzo di software libero nel nostro Paese.
